package com.halifaxparking.prj_1;

/**
 * Created by psaiprasant on 1/19/16.
 */
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;


public class Intro_Activity extends AppIntro {
    @Override
    public void init(Bundle savedInstanceState) {

        String title="1";
        String title1="2";
        String title2="3";

        String description="slide 1";
        String description1="slide 2";
        String description2="slide 3";

        int image= this.getResources().getIdentifier("ic_slide1", "drawable", this.getPackageName());
        int image1= this.getResources().getIdentifier("ic_slide2", "drawable", this.getPackageName());
        int image2= this.getResources().getIdentifier("ic_slide3", "drawable", this.getPackageName());

        int background_colour= getResources().getColor(R.color.colorAccent);
        int background_colour1= getResources().getColor(R.color.colorPrimary);
        int background_colour2= getResources().getColor(R.color.colorPrimaryDark);

        addSlide(AppIntroFragment.newInstance(title, description, image, background_colour));
        addSlide(AppIntroFragment.newInstance(title1, description1, image1, background_colour1));
        addSlide(AppIntroFragment.newInstance(title2, description2, image2, background_colour2));

        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));
    }

    private void loadMainActivity(){

        Intent i = new Intent(Intro_Activity.this, HomeScreen_Activity.class);
        this.startActivity(i);
    }

    @Override
    public void onSkipPressed() {

        loadMainActivity();
    }

    @Override
    public void onDonePressed() {

        loadMainActivity();
    }

    public void getStarted(View v){

        loadMainActivity();
    }
}