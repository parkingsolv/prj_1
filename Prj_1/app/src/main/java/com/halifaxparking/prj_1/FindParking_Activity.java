package com.halifaxparking.prj_1;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by psaiprasant on 1/19/16.
 */
@TargetApi(11)
public class FindParking_Activity extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Set title bar
        ((HomeScreen_Activity) getActivity())
                .setActionBarTitle(" ");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.list_map);
        item.setVisible(false);
    }
}
