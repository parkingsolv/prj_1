package com.halifaxparking.prj_1;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by psaiprasant on 1/19/16.
 */
@TargetApi(11)
public class Help_Activity extends Fragment {

    private TextView faqTv1;
    private TextView ppTv1;
    private Button emailBtn;
    private Button callBtn;

    String phno = "9023290792";
    String faqUrl = "http://www.google.com";
    String ppUrl = "http://www.github.com";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Set title bar
        ((HomeScreen_Activity) getActivity())
                .setActionBarTitle("Help");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.list_map);
        item.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_help, container, false);

        // for FAQ
        faqTv1= (TextView) view.findViewById(R.id.textView4);
        faqTv1.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(faqUrl));
                startActivity(intent);
                return false;
            }
        });

        // for Privacy policy
        ppTv1= (TextView) view.findViewById(R.id.textView6);
        ppTv1.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ppUrl));
                startActivity(intent);
                return false;
            }
        });

        // for call
        callBtn = (Button) view.findViewById(R.id.button);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+phno));
                startActivity(intent);
            }
        });

        // for email
        emailBtn = (Button) view.findViewById(R.id.button2);
        emailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uriText =
                        "mailto:patelkhanasaiprasant@gmail.com";

                Uri uri = Uri.parse(uriText);

                Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
                sendIntent.setData(uri);
                startActivity(Intent.createChooser(sendIntent, "Send email"));
            }
        });

        return view;
    }


}
