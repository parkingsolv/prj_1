package com.halifaxparking.prj_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by psaiprasant on 1/21/16.
 */
public class CurrentReservations_Activity extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String TAG = CurrentReservations_Activity.class.getSimpleName();

    private String URL_TOP_250 = "http://krone.cs.dal.ca/andro/specific";

    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private SwipeListAdapter adapter;
    private List<Movie> movieList;

    // initially offset will be 0, later will be updated while parsing the json
//    private int offSet = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.currentreservationstab_activity, container, false);

        if (CheckInternetConnectivity.getInstance(getActivity()).isOnline()) {

            listView = (ListView) view.findViewById(R.id.listView);
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

            movieList = new ArrayList<>();
            adapter = new SwipeListAdapter(getActivity(), movieList);
            listView.setAdapter(adapter);

            swipeRefreshLayout.setOnRefreshListener(this);

            /**
             * Showing Swipe Refresh animation on activity create
             * As animation won't start on onCreate, post runnable is used
             */
            swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            swipeRefreshLayout.setRefreshing(true);

                                            fetchMovies();
                                        }
                                    }
            );

            return view;
        } else {

            Toast.makeText(getContext(), "You are offline!!!!", Toast.LENGTH_SHORT).show();
            Log.v("Home", "############################You are not online!!!!");
        }
        return view;

    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        fetchMovies();
        movieList.clear();
    }

    /**
     * Fetching movies json by making http call
     */
    private void fetchMovies() {
        // showing refresh animation before making http call
        swipeRefreshLayout.setRefreshing(true);
        if (CheckInternetConnectivity.getInstance(getContext()).isOnline()) {
            // appending offset to url
            String url = URL_TOP_250;


            // Volley's json array request object
            JsonArrayRequest req = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());

                            if (response.length() > 0) {

                                // looping through json and adding to movies list
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject movieObj = response.getJSONObject(i);

                                        // int rank = movieObj.getInt("rank");
                                        String title = movieObj.getString("EmailId");

                                        Movie m = new Movie(title);

                                        movieList.add(0, m);

                                        // updating offset value to highest value
                                        //if (rank >= offSet)
                                        //  offSet = rank;

                                    } catch (JSONException e) {
                                        Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                    }
                                }

                                adapter.notifyDataSetChanged();
                            }

                            // stopping swipe refresh
                            swipeRefreshLayout.setRefreshing(false);


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Server Error: " + error.getMessage());

                    Toast.makeText(getContext(), "Server not responding", Toast.LENGTH_LONG).show();

                    // stopping swipe refresh
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            // Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(req);
        } else {

            Toast.makeText(getContext(), "You are offline!!!!", Toast.LENGTH_SHORT).show();
            Log.v("Home", "############################You are not online!!!!");
        }
    }

}
