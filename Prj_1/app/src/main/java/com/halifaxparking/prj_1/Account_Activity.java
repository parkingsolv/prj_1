package com.halifaxparking.prj_1;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by psaiprasant on 1/19/16.
 */
@TargetApi(11)
public class Account_Activity extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Set title bar
        ((HomeScreen_Activity) getActivity())
                .setActionBarTitle("Account");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.list_map);
        item.setVisible(false);
    }

    TextView changepwdTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_account, container, false);

        final EditText firstnameEdtxt = (EditText) view.findViewById(R.id.editText2);
        final EditText lastnameEdtxt = (EditText) view.findViewById(R.id.editText3);
        final EditText emailEdtxt = (EditText) view.findViewById(R.id.editText4);
        final EditText contactnoEdtxt = (EditText) view.findViewById(R.id.editText5);

        firstnameEdtxt.setText("First Name: ");
        //Selection.setSelection(firstnameEdtxt.getText(), firstnameEdtxt.getText().length());

        lastnameEdtxt.setText("Last Name: ");
        Selection.setSelection(lastnameEdtxt.getText(), lastnameEdtxt.getText().length());

        emailEdtxt.setText("Email: ");
        Selection.setSelection(emailEdtxt.getText(), emailEdtxt.getText().length());

        contactnoEdtxt.setText("Phone: ");
        Selection.setSelection(contactnoEdtxt.getText(), contactnoEdtxt.getText().length());

        firstnameEdtxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("First Name: ")) {
                    firstnameEdtxt.setText("First Name: ");
                    Selection.setSelection(firstnameEdtxt.getText(), firstnameEdtxt.getText().length());
                }
            }
        });

        lastnameEdtxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("Last Name: ")) {
                    lastnameEdtxt.setText("Last Name: ");
                    Selection.setSelection(lastnameEdtxt.getText(), lastnameEdtxt.getText().length());
                }
            }
        });

        emailEdtxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("Email: ")) {
                    emailEdtxt.setText("Email: ");
                    Selection.setSelection(emailEdtxt.getText(), emailEdtxt.getText().length());
                }
            }
        });

        contactnoEdtxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("Phone: ")) {
                    contactnoEdtxt.setText("Phone: ");
                    Selection.setSelection(contactnoEdtxt.getText(), contactnoEdtxt.getText().length());
                }
            }
        });

        changepwdTv= (TextView) view.findViewById(R.id.textView12);
        changepwdTv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Changepassword_Activity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
