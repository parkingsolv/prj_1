var router     = require('express').Router(),
    db         = require('./connectdb')(),
    formidable = require('formidable'),
    fs         = require('fs-extra'),
    util       = require('util'),
    path       = require('path');

db.connect(function(err) {
    if (err) { console.error(err.stack); return;}
});

router.use(function(req, res, next) {
    console.log('--new request--');
    next(); // visit next routes
});

//ANDROID General routes
router.get('/andro', function(req, res) {
    res.json({ message: 'Halifax parking system.' });   
});

//To return rows
router.get('/andro/specific', function(req, res) {

            db.query('select EmailId from UserDetails', function(err, rows, fields) {
                if (err) throw err;
                
                if (rows.length > 0) {

                    res.json(rows);
                }else {
                    res.json({message: "error"});
                }
            }); 
});

// For UserDetails table
// LOGIN Route
router.route('/andro/login').post(function(req, res) {
    var tag      = req.body.tag,
        email    = req.body.email,
        password = req.body.password;

    req.checkBody('email', 'not a valid email.').isEmail();
    req.checkBody('password', '8-32 character').len(8, 32);
    var errors = req.validationErrors();

    if (tag == "login") {
        if (errors) {
            res.json({message: errors.msg});
        }else {
            db.query('select * from UserDetails where EmailId = ? and UsrPwd = ?', [email, password], function(err, rows, fields) {
                if (err) throw err;
                
                if (rows.length > 0) {
                    userID  = rows[0].UserId + "";
                    res.json({success: "1", userid: userID, message: "user logged in"});
                }else {
                    res.json({success: "0", message: "invalid email or password"});
                }
            });
        }
    }else {
        res.json({success: "2", message: "not a login request"});
    }
    
});





// For UserDetails table
// REGISTER Route
router.route('/andro/register').post(function(req, res) {
    var tag      = req.body.tag,
	firstname = req.body.firstname,
	lastname = req.body.lastname,
        email    = req.body.email,
        password = req.body.password,
	contactno = req.body.contactno;

    req.checkBody('firstname', 'Field blank.').notEmpty();
    req.checkBody('lastname', 'Field blank.').notEmpty();
    req.checkBody('email', 'not a valid email.').isEmail();
    req.checkBody('password', '8-32 character').len(8, 32);
    req.checkBody('contactno', 'Field blank').notEmpty();
    var errors = req.validationErrors();

    if (tag === "register") {
        if (errors) {
            res.json({message: errors});
        }else {
        //check whether user exists
        db.query('select * from UserDetails where EmailId = ?', [email], function(err, rows, result) {
            if (err) throw err;
            if(rows.length > 0){
                res.json({success: "0", message: "email exists"});
            }else {
                db.query('INSERT INTO UserDetails VALUES(default, ?, ?, ?, ?, ?, now())', [firstname, lastname, email, password, contactno], function(err, result) {
                          if (err) throw err;
                          res.json({success: "1", userID: result.insertId, message: "user registered"});
                          console.log(result.insertId);
                        });
            }
        });
        }

    }else {
        res.json({success: "2", message: "not a register request"});
    }

});

//For UserDetails table
//UPDATE Route
router.route('/andro/update').post(function(req, res) {
    var tag      = req.body.tag,
        firstname = req.body.firstname,
        lastname = req.body.lastname,
        email    = req.body.email,
        password = req.body.password,
        contactno = req.body.contactno;

    req.checkBody('firstname', 'Field blank.').notEmpty();
    req.checkBody('lastname', 'Field blank.').notEmpty();
    req.checkBody('email', 'not a valid email.').isEmail();
    req.checkBody('password', '8-32 character').len(8, 32);
    req.checkBody('contactno', 'Field blank').notEmpty();
    var errors = req.validationErrors();

    if (tag === "update") {
        if (errors) {
            res.json({message: errors});
        }else {
	db.query('UPDATE UserDetails SET FirstName = ?, LastName = ?, EmailId = ?, UsrPwd = ?, ContactNo = ? WHERE UserId = ?', [firstname, lastname, email, password, contactno], function(err, result) {
                    if (err) throw err;
                    res.json({success: "1", userID: result.insertId, message: "updated."});
                    console.log(result.insertId);
                });
        }}else {
        res.json({success: "2", message: "not an update request"});
        }
});


//DELETE Route
router.route('/andro/delete').post(function(req, res) {
    var tag      = req.body.tag,
        username = req.body.username,
        email    = req.body.email,
        password = req.body.password;

    req.checkBody('email', 'not a valid email.').isEmail();
    req.checkBody('username', '2-32 character').len(2, 32);
    req.checkBody('password', '8-32 character').len(8, 32);
    var errors = req.validationErrors();

    if (tag === "delete") {
        if (errors) {
            res.json({message: errors});
        }else {
	db.query('DELETE FROM users WHERE email = ?', [email], function(err, result) {
                    if (err) throw err;
                    res.json({success: "1", userID: result.insertId, message: "Account deleted"});
                    console.log(result.insertId);
                });
        }}else {
        res.json({success: "2", message: "not a delete request"});
        }
});


module.exports.router = router;
